package com.tidiany.ms3graphql.web.controller;

import com.tidiany.ms3graphql.service.EtudiantService;
import com.tidiany.ms3graphql.web.dto.EtudiantDto;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class EtudiantController {
    private final EtudiantService etudiantService;

    public EtudiantController(EtudiantService etudiantService) {
        this.etudiantService = etudiantService;
    }

    @QueryMapping
    public List<EtudiantDto> getAllEtudiants() {
        return etudiantService.getAll();
    }

    @QueryMapping
    public EtudiantDto getById(@Argument Long id) {
        return etudiantService.getById(id);
    }
}
