package com.tidiany.ms3graphql.web.dto;

public record EtudiantDto(Long id, String prenom, String nom, AdresseDto adresse, String telephone) {
}
