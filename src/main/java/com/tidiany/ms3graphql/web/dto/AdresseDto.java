package com.tidiany.ms3graphql.web.dto;

public record AdresseDto(Long id, Integer num_rue, String nom_rue, String cite, String pays) {
}
