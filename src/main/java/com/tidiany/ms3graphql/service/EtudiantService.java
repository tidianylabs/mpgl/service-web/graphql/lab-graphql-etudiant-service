package com.tidiany.ms3graphql.service;

import com.tidiany.ms3graphql.web.dto.AdresseDto;
import com.tidiany.ms3graphql.web.dto.EtudiantDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class EtudiantService {

    private static List<EtudiantDto> etudiantDtos = List.of(
            new EtudiantDto(1L, "Tidiany", "Toure", new AdresseDto(1L, 1, "", "Cite Tacko", "Sénégal"), "778876654"),
            new EtudiantDto(2L, "Moussa", "Diallo", new AdresseDto(2L, 2, "", "Mariste", "Sénégal"), "778876654"),
            new EtudiantDto(3L, "You", "Ndiaye", new AdresseDto(3L, 3, "", "Cite Fadia", "Sénégal"), "778876654"),
            new EtudiantDto(4L, "Mac", "Ndao", new AdresseDto(4L, 4, "", "Yeumbeul", "Sénégal"), "778876654"),
            new EtudiantDto(5L, "Arrona", "Sarr", new AdresseDto(5L, 5, "", "Boune", "Sénégal"), "778876654")
    );

    public List<EtudiantDto> getAll() {
        return etudiantDtos;
    }

    public EtudiantDto getById(Long id) {
        return etudiantDtos.stream().filter(e -> Objects.equals(e.id(), id)).toList().get(0);
    }
}
